import CoreLocation

extension CLLocation {
    var latLng: String {
        return "\(self.coordinate.latitude),\(self.coordinate.longitude)"
    }
}
