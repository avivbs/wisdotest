
import CoreLocation
import MapKit

protocol AddressServiceDelegate: class {
    var timeForUpdateInSeconds: Double { get }
    func addressUpdated(address: String)
}

struct GoogleGeocoding {
    static let apiKey = "AIzaSyB2xmlzOnWmkxldsFia2W_teWDFnNBEZqE"
    static let reverseGeocodeEndpoint = "https://maps.googleapis.com/maps/api/geocode/json"
}

class AddressService {
    
    weak var delegate: AddressServiceDelegate?
    
    var timer: Timer?
    
    var location: CLLocation? {
        didSet {
            if oldValue == nil {
                requestAddressIfNeeded()
                start()
            }
        }
    }
    
    var previousLocation: CLLocation?
    
    func start() {
        guard let updateTime = delegate?.timeForUpdateInSeconds else { return }
        self.timer = Timer.scheduledTimer(timeInterval: updateTime, target: self, selector: #selector(requestAddressIfNeeded), userInfo: nil, repeats: true)
    }
    
    @objc func requestAddressIfNeeded() {
        guard let current = location else { return }
        guard previousLocation == nil || current.distance(from: previousLocation!) > 0 else { return }
        var params = [String : String]()
        params["latlng"] = current.latLng
        params["key"] = GoogleGeocoding.apiKey
        self.previousLocation = current
        API.request(method: .get, url: GoogleGeocoding.reverseGeocodeEndpoint, queryParams: params) { [weak self] result in
            guard let json = result.json else {
                print("error in request for address: \(result.error ?? "")")
                self?.previousLocation = nil
                return
            }
            if let address = json["results"].array?.first?["formatted_address"].string {
                self?.addressUpdated(address)
            }
        }
    }
    
    func addressUpdated(_ newAddress: String) {
        delegate?.addressUpdated(address: newAddress)
    }
    
    func stop() {
        self.timer?.invalidate()
    }
}
