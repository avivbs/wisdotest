
import CoreLocation
import MapKit

protocol PlacesServiceDelegate: class {
    var showPlacesInRadius: Double { get }
    var distanceForUpdateInMeters: Double { get }
    func placeListUpdated(places: [PlaceDataModel])
}

struct GooglePlaces {
    static let apiKey = "AIzaSyAu-5TXjykUdgvqY8nmCYaMtPvAF9R4UX8"
    static let nearbyEndpoint = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
}

class PlacesService {

    weak var delegate: PlacesServiceDelegate?
    
    var previousLocation: CLLocation?
    
    var location: CLLocation? {
        didSet {
            guard let location = location, let delegate = delegate else { return }
            if previousLocation == nil || previousLocation!.distance(from: location) >= delegate.distanceForUpdateInMeters {
                self.getNearbyPlaces()
            }
        }
    }
    
    func getNearbyPlaces() {
        guard let location = location, let delegate = delegate else { return }
        var params = [String : String]()
        params["location"] = location.latLng
        params["radius"] = "\(delegate.showPlacesInRadius)"
        params["key"] = GooglePlaces.apiKey
        self.previousLocation = location
        API.request(method: .get, url: GooglePlaces.nearbyEndpoint, queryParams: params) { [weak self] result in
            guard let json = result.json else {
                print("error in places request: \(result.error ?? "")")
                self?.previousLocation = nil
                return
            }
            var places = [PlaceDataModel]()
            for placeJson in json["results"].arrayValue {
                places.append(PlaceDataModel(json: placeJson))
            }
            self?.delegate?.placeListUpdated(places: places)
        }
    }
    
    func distance(from coordinates: CLLocationCoordinate2D) -> Double {
        return self.location!.distance(from: CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude))
    }
}
