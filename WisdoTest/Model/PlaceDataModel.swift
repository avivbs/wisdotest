import SwiftyJSON
import CoreLocation

class PlaceDataModel: CustomStringConvertible  {
    
    var id: String
    var name: String
    var type: String
    var location: CLLocationCoordinate2D
    var open: Bool
    
    var displayType: String {
        get {
            return type.replacingOccurrences(of: "_", with: " ").capitalized
        }
    }
    
    var description: String {
        var desc = "name: \(name)\n"
        desc += "location: \(location)\n"
        desc += "type: \(type)\n"
        desc += "open: \(open)\n"
        return desc
    }
    
    init(id: String, name: String, type: String, location: CLLocationCoordinate2D, open: Bool = false) {
        self.id = id
        self.name = name
        self.type = type
        self.location = location
        self.open = open
    }
    
    init(json: JSON) {
        self.id = json["place_id"].stringValue
        self.name = json["name"].stringValue
        self.type = json["types"].array?[0].string ?? ""
        let locationDictionary = json["geometry"]["location"].dictionaryValue
        self.location = CLLocationCoordinate2D(latitude: locationDictionary["lat"]!.doubleValue, longitude: locationDictionary["lng"]!.doubleValue)
        self.open = json["opening_hours"]["open_now"].bool ?? false
    }
}
