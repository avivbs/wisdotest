
import CoreLocation
import MapKit

protocol LocationServiceDelegate: class {
    var distanceForUpdateInMeters: Double { get }
    func locationUpdated(location: CLLocation)
}

class LocationService: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    weak var delegate: LocationServiceDelegate? {
        didSet {
            if delegate != nil {
                self.start()
            }
        }
    }
    
    var recentLocation: CLLocation? {
        didSet {
            if let location = recentLocation {
                delegate?.locationUpdated(location: location)
            }
        }
    }
    
    func start() {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            requestLocationUpdates()
        }
    }
    
    func stop() {
        locationManager.stopUpdatingLocation()
    }
    
    func requestLocationUpdates() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = delegate?.distanceForUpdateInMeters ?? kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
    }
    
    // MARK: CLLocationManagerDelegate methods
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if [.authorizedWhenInUse, .authorizedAlways].contains(status) {
            requestLocationUpdates()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = manager.location {
            self.recentLocation = location
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error: \(error)")
    }
}
