import MapKit

protocol MapPresenterView: class {
    func setLocation(region: MKCoordinateRegion)
    func setAddress(address: String)
    func setPlaces(places: [PlaceViewData])
}

struct PlaceViewData {
    var name: String
    var type: String
    var distance: String
    var open: Bool
    var coordinates: CLLocationCoordinate2D
}

class MapPresenter: LocationServiceDelegate, AddressServiceDelegate, PlacesServiceDelegate {
    
    let regionRadius: CLLocationDistance = 500 // Radius from center in meters
    var distanceForUpdateInMeters: Double = 50 // LocationServiceDelegate
    var timeForUpdateInSeconds: Double = 15 // AddressServiceDelegate
    var showPlacesInRadius: Double = 1000 // PlacesServiceDelegate
    
    weak private var mapPresenterView: MapPresenterView?
    
    let locationService: LocationService = LocationService()
    let addressService: AddressService = AddressService()
    let placesService: PlacesService = PlacesService()
    
    func attach(mapView: MapPresenterView) {
        self.mapPresenterView = mapView
        locationService.delegate = self
        addressService.delegate = self
        placesService.delegate = self
    }
    
    func locationUpdated(location: CLLocation) {
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapPresenterView?.setLocation(region: region)
        addressService.location = location
        placesService.location = location
    }
    
    func addressUpdated(address: String) {
        mapPresenterView?.setAddress(address: address)
    }
    
    func placeListUpdated(places: [PlaceDataModel]) {
        let placeViewDataArray = places.map {
            return PlaceViewData(name: $0.name, type: $0.displayType, distance: "\(String(format: "%.2f", placesService.distance(from: $0.location) / 1000)) KM", open: $0.open, coordinates: $0.location)
            }.sorted(by: { $0.distance < $1.distance })
        mapPresenterView?.setPlaces(places: placeViewDataArray)
    }
    
    func detach() {
        locationService.stop()
        addressService.stop()
        self.mapPresenterView = nil
    }
}
