
import UIKit
import MapKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MapPresenterView {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var placesTableView: UITableView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    private let presenter = MapPresenter()
    
    private var datasource = [PlaceViewData]() {
        didSet {
            placesTableView.reloadData()
            addAnnotationsToMap()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(mapView: self)
    }
    
    // MARK: MapPresenterView Protocol
    
    func setLocation(region: MKCoordinateRegion) {
        mapView.setRegion(region, animated: true)
    }
    
    func setAddress(address: String) {
        spinner.stopAnimating()
        addressLabel.text = address
    }
    
    func setPlaces(places: [PlaceViewData]) {
        datasource = places
    }
    
    // MARK: IBActions
    
    @IBAction func centerMap(_ sender: UIButton) {
        mapView.setCenter(mapView.userLocation.coordinate, animated: true)
    }
    
    // MARK: UITableViewDelegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCellIdentifier", for: indexPath) as! PlaceCellView
        let place = datasource[indexPath.row]
        cell.nameLabel.text = place.name
        cell.typeLabel.text = place.type
        cell.distanceLabel.text = place.distance
        cell.openLabel.isHidden = !place.open
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mapView.setCenter(datasource[indexPath.row].coordinates, animated: true)
    }
    
    func addAnnotationsToMap() {
        var annotations = [MKPointAnnotation]()
        for place in datasource {
            let annotation = MKPointAnnotation()
            annotation.coordinate = place.coordinates
            annotation.title = place.name
            annotations.append(annotation)
        }
        mapView.addAnnotations(annotations)
    }
    
    // MARK: Deinit
    
    deinit {
        presenter.detach()
    }
}

class PlaceCellView: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var openLabel: UILabel!
}
